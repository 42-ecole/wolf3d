NAME := wolf3d
NPWD := $(CURDIR)/$(NAME)

CC := gcc -march=native -mtune=native -flto -Ofast -pipe
CC_DEBUG := gcc -march=native -mtune=native -g3
CFLAGS := -Wall -Wextra -Werror -Wunused
IFLAGS := -I $(CURDIR)/includes/ -I $(CURDIR)/utils/libft/includes
LIBSINC :=
LIBS :=
GNL_DIR = $(addprefix ./utils/gnl/, $(GNL))
GNL = get_next_line.c
UNAME_S := $(shell uname -s)
ifeq ($(UNAME_S),Darwin)
	LIBSINC += -I ~/.brew/include
	LIBS += -L ~/.brew/lib -rpath ~/.brew/lib
endif
LIBS += -F ~/Library/Frameworks -framework SDL2 -F ~/Library/Frameworks/ -framework SDL2_mixer

SRC := srcs/main.c \
	srcs/color.c \
	srcs/functions.c \
	srcs/reader.c \
	srcs/sounds.c \
	srcs/time.c \
	srcs/controlls/controls.c \
	srcs/controlls/key.c \
	srcs/controlls/movements.c \
	srcs/init/init_adv.c \
	srcs/init/init.c \
	srcs/loader/assets_loader.c \
	srcs/loader/errors.c \
	srcs/loader/options.c \
	srcs/loader/tga_reader.c \
	srcs/render/draw_adv.c \
	srcs/render/draw.c \
	srcs/render/image.c \
	srcs/render/render.c \
	srcs/ui/draw_ui.c \
	srcs/ui/menu.c \
	srcs/ui/ui.c \
	srcs/weapon/riffle_fraps.c \
	srcs/weapon/riffle.c

OBJ := $(SRC:.c=.o)
OBJS = $(addprefix $(OBJ_DIR)/,$(SRCS:.c=.o))
	OBJ_DIR = objs

LIBFT := $(CURDIR)/utils/libft/libft.a
LMAKE := make -sC utils/libft

DEL := rm -rf

WHITE := \033[0m
BGREEN := \033[42m
GREEN := \033[32m
RED := \033[31m
INVERT := \033[7m

SUCCESS := [$(GREEN)✓$(WHITE)]
SUCCESS2 := [$(INVERT)$(GREEN)✓$(WHITE)]

all: $(NAME);

$(OBJ): %.o: %.c
	@echo -n ' $@: '
	@$(CC) -c $(CFLAGS) $(LIBSINC) $(IFLAGS) $< -o $@
	@echo "$(SUCCESS)"

install:
	@if [ ! -f ~/.brewconfig.zsh ]; \
	then \
		curl -fsSL https://rawgit.com/kube/42homebrew/master/install.sh | zsh; \
		brew install sdl2; \
		brew install sdl2_mixer; \
	fi;
	
$(LIBFT):
	@$(LMAKE)

$(NAME): install $(LIBFT) $(OBJ)
	@echo -n ' <q.p> | $(NPWD): '
	@$(CC) $(OBJ) $(LIBS) $(GNL_DIR) $(LIBFT) -o $(NAME)
	@mkdir -p $(OBJ_DIR) && mv $(SRC:.c=.o) $(OBJ_DIR)
	@echo "$(SUCCESS2)"

del:
	@$(DEL) $(OBJ)
	@$(DEL) $(NAME)

pre: del all
	@echo "$(INVERT)$(GREEN)Successed re-build.$(WHITE)"

$(CC_DEBUG):
	@$(eval CC=$(CC_DEBUG))
	debug_all: $(CC_DEBUG) pre
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(WHITE)"
	debug: $(CC_DEBUG) all
	@echo "$(INVERT)$(NAME) $(GREEN)ready for debug.$(WHITE)"

clean:
	@$(DEL) $(OBJ) $(OBJ_DIR)
	@$(LMAKE) clean

fclean: clean
	@$(LMAKE) fclean
	@$(DEL) $(NAME)
	@echo "$(INVERT)$(RED)deleted$(WHITE)$(INVERT): $(NPWD)$(WHITE)"

re: fclean all

.PHONY: all fclean clean re pre debug debug_all set_cc_debug install
